import py5


def setup():
    py5.full_screen()


def draw():
    py5.fill(
        py5.random_int(255),  # red
        py5.random_int(255),  # green
        py5.random_int(255),  # blue
    )

    py5.arc(
        py5.mouse_x,
        py5.mouse_y,
        py5.random(80),
        py5.random(80),
        py5.random(5),
        py5.PI+py5.QUARTER_PI,
        py5.PIE
    )


py5.run_sketch()
