import py5


def setup():
    py5.full_screen()


def draw():
    py5.fill(
        py5.random_int(255),  # red
        py5.random_int(255),  # green
        py5.random_int(255),  # blue
    )
    for _ in range(100):
        py5.circle(
            py5.random_int(py5.width),
            py5.random_int(py5.height),
            py5.random(100)
        )


def exiting():
    py5.save('drawings/24/hello_word/shot.png')


py5.run_sketch()
