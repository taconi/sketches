gradient()
  .mult(osc(3), () => blend)
  .rotate(($) => time % 240)
  .color([5, 15, -20].fast(0.51), [2, 0, 95].fast(1), [5, -4, -1].fast(0.5))
  .rotate(0.5)
  .pixelate(-2, 15)
  .repeat(() => 10 * Math.sin(time * 0.2))
  .kaleid(100)
  .out();
