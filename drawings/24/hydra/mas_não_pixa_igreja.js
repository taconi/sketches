gradient().out(o1)


osc(15,.02,8)
.add(gradient().luma())
.colorama(1)
.luma()
.luma([.5,.6,.7,.8,.9,1,1.1,1,.9,.8,.7,.6].smooth(.25).fast(4))
.brightness(-.2)
.add(
  src(o1)
  .luma([-2, -1, 0, 1, 2].smooth(.5).fast(.5))
  .brightness(-.2)
  .colorama(1)
  .scale(.9)
  .rotate(2,.2)
  .add(voronoi(8, .01, 7))
)
.kaleid([4,5,6,7,8,7,6,5].smooth(.35))
.modulate(noise(3),()=>a.fft[0])
.out(o0)
// hush()


setResolution(500,500)
