osc(20,.04, 12)
  .modulateKaleid(
    shape([3,4,5,6,7,8].fast(8))
    .modulateScale(osc())
  )
  .modulateRotate(
    gradient(1,4)
    .repeat(100,100)
  )
  .diff(voronoi().repeat([10, 20, 40],[10, 20, 40]))
  .out()
