voronoi([10,20,30],.04,[10,5.5,21].fast(2))
  .color(1,4,2)
  .modulateScale(
    osc().scale(2),
    [.5, 1].smooth(.25).fast(2)
  )
  .modulateKaleid(gradient(), 30)
  .modulateRepeat(osc(), 1)
  .scale(.5)
  .kaleid(8)
  .modulateKaleid(
    shape([3,4].fast(4).smooth(.25))
    .modulateScale(osc(),.5)
    .repeat(()=>a.fts[3],()=>a.fts[2])
    .rotate(()=>time/10)
    .scale(()=>a.fts[1])
  )
  .out(o1)


osc(1, 3, 1)
  .diff(
    voronoi(4, 0.4, 3)
    .rotate(()=>time/20)
    .repeatX(9)
    .modulateKaleid(osc(), 3)
  )
  .scrollX(10)
  .diff(src(o1))
  .out()
