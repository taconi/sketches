await loadScript('https://hyper-hydra.glitch.me/hydra-text.js')
str=`
CONTRATOS
`


voronoi(40,.1,4)
  .color(.2,-.4,-2)
  .diff(
    shape(4)
    .modulateRepeat(osc(10,0.1),.3)
    .repeatY(10)
    .rotate(()=>time/20)
    .kaleid([3, 5, 7, 9].smooth(.25).fast(.75))
  )
  .add(
    gradient(1,.2)
    .brightness(-.5)
	.layer(
      text(str).color(-1,0,1)
      .rotate(()=>time/20)
    )
	.diff(
      strokeText(str)
      .modulateScale(noise(1,1), .4)
    )
    .add(
      strokeText(str)
      .modulateScale(
        noise(1,.4), .5
      )
      // .rotate(1)
    )
  )
  .out()
