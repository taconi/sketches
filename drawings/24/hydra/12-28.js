// https://hydra.ojack.xyz/?sketch_id=yJpXOjywYxK8SFZ7

bpm=120

voronoi(
  [60,40,80,100].smooth(1).fast(2),
  0.1,
  [9,5,2].fast(.5).smooth(.15),
)
.modulateRotate(osc())
.modulateKaleid(
  shape(666)
  .rotate(()=>time/20),
3)
.blend(osc())
.luma(-.1)
.hue(-2)
.colorama(2.2)
.color([3,4],[1,2, 5].smooth(.4),0)
.out(o1)


osc(9, 0.2, 9)
.diff(src(o0))
.diff(gradient(9), .2)
.modulateKaleid(voronoi(), 8)
.out(o0)
