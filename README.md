# Sketches

> Brincadeiras bobas e gostosas.

---

Aqui você encontrará algumas viagens e experimentações com as bibliotecas
[py5](https://py5coding.org), [sardine](https://sardine.raphaelforment.fr),
[FoxDot](https://github.com/Qirky/FoxDot),
[renardo](https://github.com/e-lie/renardo) e [hydra](https://github.com/hydra-synth/hydra).

## Dependências

Use o [pipx](https://pipx.pypa.io) para instalar o [poetry](https://github.com/python-poetry/poetry)
com o comando abaixo:

```shell
pipx install poetry
```

Crie um ambiente virtual e instale as bibliotecas para brincar com os sons e os rabiscos:

```shell
poetry install
```

Entre no ambiente virtual:

```shell
poetry shell
```

## py5

Os arquivos estão dentro da pasta `drawings` com o padrão: `drawings/{ano}/py5/{rabisco}/draw.py`.

Para ver os rabiscos com o py5 use o comando `poe py5`.

Executando o `hello_word`:

```shell
poe py5
```

Executando o "draw.py" localizado em outra pasta:

```shell
poe py5 -r hello_word
```

Executando o "draw.py" localizado em outra pasta de outro ano:

```shell
poe py5 -a 24 -r hello_word
```

## [sardine](https://github.com/Bubobubobubobubo/Sardine)

Os arquivos estão dentro da pasta `musics` com o padrão: `musics/{ano}/sardine/{som}.py`.

Para viajar nos sons do sardine use o comando `poe sardine`.

## [FoxDot](https://github.com/Qirky/FoxDot)

Os arquivos estão dentro da pasta `musics` com o padrão: `musics/{ano}/foxdot/{som}.py`.

Para viajar nos sons do FoxDot use o comando `poe fox`.

## [renardo](https://github.com/e-lie/renardo)

Os arquivos estão dentro da pasta `musics` com o padrão: `musics/{ano}/renardo/{som}.py`.

Para viajar nos sons do renardo use o comando `renardo`.
Irá ser aberto uma aplicação no seu terminal, faça o download dos samples e
inicie o SuperCollider por essa aplicação.

Você pode abir o FoxDotEditor por ela ou abrir o editor [pulsar](https://pulsar-edit.dev/)
para usar o plugin [pulsardo](https://github.com/e-lie/renardo-pulsar-extension).
Ou usar o flok como esta descrito abaixo.

## [flok](https://github.com/munshkr/flok)

[Sessão](https://flok.cc/s/taconi) pública do flok,
as vezes estou por lá.

Caso queira testar você precisa instalar o nodejs 18 ou posterior e instalar o
`npx` com o comando:

```shell
npm install -g npx
```

Para conectar-se ao flok use o comando `poe flok`.

Altere o hub que por padrão é: wss://flok.cc

No README.md do flok tem uma lista de servidores públicos conhecidos

https://github.com/munshkr/flok?tab=readme-ov-file#public-server

```shell
  $ poe flok -h wss://flok.cc
```

Altere a sessão que por padrão é: saci-session

É necessário criar uma sessão no hub.

```shell
poe flok -s saci-session
```

Altere o tipo que por padrão é: renardo

Algumas opções possíveis:
renardo | foxdot | sardine

```shell
  $ poe flok -t sardine
```

Altere o usuário que por padrão é: taconi

Caso queira compartilhar a sessão

```shell
  $ poe flok -u patamon
```

## [hydra](https://github.com/hydra-synth/hydra)

Os arquivos estão dentro da pasta `drawings` com o padrão: `drawings/{ano}/hydra/{viagem}.py`.

Caso você tenha instalado o editor [pulsar](https://pulsar-edit.dev/), além de
codar com o renardo você também pode usar o pacote [atom-hydra](https://github.com/hydra-synth/atom-hydra)
para codar com hydra.

Você também pode abrir uma sessão qualquer no flok e colar o conteúdo do arquivo lá.
Aqui está uma [sessão](https://flok.cc/s/taconi) pública do flok de exemplo.

## [strudel](https://strudel.cc)

Os arquivos estão dentro da pasta `musics` com o padrão: `musics/{ano}/strudel/{viagem}.py`.

Você pode abrir o site [https://strudel.cc](https://strudel.cc) para editar o código
ou abrir uma sessão qualquer no flok e colar o conteúdo do arquivo lá.
Aqui está uma [sessão](https://flok.cc/s/taconi) pública do flok de exemplo.
