mix = MidiIn(1)
mix.load_midimap(0)
mix.load_valmap('/home/matos/valmap.json', 3)
mix.run()

mix.set_value('U0:E0', 'd1.sus', [1/3, 1, 0])
mix.set_value('U0:E3', 'd1.amp', [1, 0])
mix.set_value('U0:E3', 'd_all.amp', [1, 0])

mix.set_value('U1:E0', 'c1.room', [1/16, 1.5, .2])
mix.set_value('U1:E0', 'c1.size', [1/16, 1.5, .2])

mix.set_value('U1:E0', 'c1.attack', [1/3, 4, .1])

mix.set_value('U1:E1', 'c1.vib', [1/32, 2, .1])

# mix.del_value('U1:E1', 'c1.vib')

mix.set_value('U1:E2', 'c1.vibdepth', [1/3, 16, 1])

mix.set_value('U0:E5', 'm1.amp', [0, 1, .1])

mix.set_value("U2:E0", 'm1.slide', [0, 2, .1])
mix.set_value("U2:E0", 'b1.slide', [0, 2, .1])

mix.set_value('U2:E1', 'b1.bend', [0, 2, .1])
mix.set_value('U2:E2', 'b1.crush', [0, 16, .1])

mix.set_value('U2:E2', 'b1.crush', [0, 16, .1])

# mix.stop()
# mix.close()



Clock.bpm=150

d1 >> play('xxo-', dur=var([1,1/2], [2,1]))
d1.sometimes('bubble').every(16, 'stutter')

c1 >> space(
  c[
  [c*'Cm5-@', 'Cm9@'], 'D7@',
  [c^['Fm6@ A7/9@', .5], 'Fdim@ E7/9@'],
  ],
  dur=PDur(12,16)*4,
  sus=[.3,.5],
  room=.4,size=1,vib=2,vibdepth=[2,1],
)

# Group(c1,d1).solo()

b1 >> jbass([P^[1, 3, var([.5, .33], [3,1])], 5], dur=4, sus=2)

d2 >> play('------(k[|k2|k])')
d3 >> play('S...')
d4 >> play('{P|p2|}.')
d5 >> play('<!><(C.)>...',dur=4,amp=.4)

m1 >> charm(
  c1.degree, amp=1, formant=[4,2,1,5],
  vib=.4, slide=.5,
  attack=expvar([.4,.7,1], 3),
)

# m1.stop()
