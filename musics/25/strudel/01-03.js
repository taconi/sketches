// https://strudel.cc/?ujHGux8TeNl0

setcps(.5)

d1: s("bd -@2 bd -@2 [bd,ht] - ht - bd - bd -@3")
  .color("brown").bank("<AJKPercusyn!3 YamahaRX5>")
  .sometimes(ply("2"))
  .room("[.75 1]").size(".75 1.25")
  .echo("<4!4 2!2>", "10", "<.5 1 3>").gain(".5")
  .delay("<0!32 .33!4>")
d2: s("cp(<<11 10 13> 12>,16)").slow("2")
  .color("brown").bank("YamahaRX21")
  .room(".25").size(".5").degradeBy(".2")
  .pan("<<0.5 -1> 1>")

p1: chord(`<
Cm9@2 Ab7 G7b13
Cm9@2 Ab7 G7b13
Cm9@2 Ab7 [Gm7 Gbm7]
Fm7@2 Dm7b5 G7b13
>(3,<11!16 12!16>)`)
  .layer(
    x=>x.voicing().s("piano").lpf("2000")
      .color("cyan").delay(".4")
      .decay(".5").attack(".5").legato(2)
      .size("<2 .9 4>").sometimes(jux(rev))
      .rarely(x=>x.room(".75"))
      .rarely(ply("4"))
    ,
    x=>x.s("gm_acoustic_bass")
      .color("magenta").fast(4).speed("-.9")
      .gain("<1.5!14 .75!2>")
      .struct(".5")
      .ply("<2 4>").euclid("12", "16")
    )
