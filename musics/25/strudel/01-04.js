// https://strudel.cc/?DWhcLvKS_v0q

await initHydra()

s0.initImage('https://i.ibb.co/XXRFzZT/a485ed4602601864.jpg')
s1.initImage('https://i.ibb.co/Wgm74Bd/pru.png')


osc(9, .25, -20)
.diff(
  src(s0)
  .repeat()
  .scrollY(.5,.1)
  .modulateScrollX(osc(80,0.1), .5,[.02, .2].fast(.25))
  .modulateHue(gradient(), 10)
  .luma(-1)
)
.mult(voronoi(9, 0.1, 90), -1)
.mult(
  src(s1)
  .modulateHue(voronoi(90, .1, 20))
  .modulateScale(voronoi(),.25)
  .scrollX(.5,.1)
  .scrollY(2,.5)
  .scale(.25)
  .modulatePixelate(voronoi(40, 2, 100), 500,500)
)
.out()

///////////////////////////////////////

setcps(.36)

samples('shabda:caos:4')
samples('shabda/speech/pt-BR/m:palavras,presas')
samples('shabda/speech/pt-BR/m:algo_assim')
samples('shabda/speech/pt-BR/f:vai_carai,ai')
samples('shabda/speech/pt-BR/f:temos_que_romper')
samples('shabda/speech/pt-BR/f:corre,avoa')

d1: s("bd -@2 bd -@2 [bd,ht] - ht - bd - bd -@3")
  .color("cyan").bank("<AJKPercusyn!3 YamahaRX5>")
  .room("[.75 1]").size("<.75 1.25>(3,8)")
  .delay("<0!32 .5!4>")

d2: s("<bd sd <oh rd> sd>*8")
  .color("magenta")
  .bank("XdrumLM8953")

p1: chord("<Am!2 G^7!2 F^7!2 E7b13!2>(<3 12>,<8 16>)")
  .voicing()
  .fast(2)
  .lpf("<1000 2000>")
  .crush("<5!8 8!3 4!2>")
  .rarely(chop("3"))
  .s("<gm_piano,- gm_vibraphone>")
  .color("yellow")
  .gain(".75")

v1: s(`
palavras <presas algo_assim <- vai_carai>>(3,8),
<- temos_que_romper@4 ai@4 corre@4>,
<-@4 caos@8>,
avoa@2
`)
  .rarely(ply("4"))
  .jux(rev)
  .hpf("700")
  .echo("1", "8/4", "2")
  .vib("12").vibmod("<.25 .5 1 2>")
  .speed("<-1!4 2!4>")
  // .gain(".75")
  .rev()
