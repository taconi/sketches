import FoxDotChord

Clock.bpm = 80

chords = c[
    c*('Bm@', ['G7M', 'Gm7M'], 'Em@', c*['D#9 D#6 D#7 D#7M D#7/9 D#9+']),
    c^('Am7/2@ C6@', 2, [4, 6], ['Am7 C6'], .25)
]
~a1 >> piano(chords, dur=4)

b1 >> play('S ', dur=4, room=.5, mix=.5, hpf=800)
b2 >> play(' b', dur=4, room=.5, mix=.5, hpf=800, lpf=linvar([1_000,7_000], 50))

b3 >> play('<Vi><(: )i><(PA|T1|(|S2|E))i>', dur=PDur(7, 11)*2)

# ---

~a1 >> pluck(
    c[
        c^['C7M@ C6@ C9+@ C9@', .25],
        c^['D7M@ D6@ D9@ D2@', .25],
        c^['C7M@ C9@ C13@ C11+@', .25],
        c^['A7M@ A9@ G#7M@ G7M@', .25],
    ],
    dur=4
)
b3.hpf = 1_000
b3.lpf = var([3_000, 7_000], [12, 4])
