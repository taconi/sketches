from FoxDotChord import PChord as c

Clock.bpm = 70

var.chords = c[
    'Am7M/9!2, F7/11+!2, Bm7/5-!2, E7/13-!2,' * 3 +
    'Am7M/9!2, F7/11+!2, Dm7/9!2, G7/4/9, F7/11+,' +
    'Em7/b-!4, A7/13-!2, Eb7/9!2,' +
    'Dm7/9!4, G7/4/9!2, Db7/9+!2,' +
    'C7M/5+!2, C7M!2, F7/11+!4,' +
    'Bm7/5-!4, E7/13-!4,' +
    'Am7M/9!2, F7/11+!2, Bm7/5-!2, E7/13-!2'
]
v1 >> pasha(var.chords, dur=PDur(3, 8), lpf=linvar([500, 1000, 500], 50))

p1 >> play('P', room=.7, mix=.5)

p2 >> play('S({!b} )', room=.7, mix=.5, lpf=linvar([1_000, 5_000], [50, 100]))

p3 >> play('--')

p4 >> play('VpOe', hpf=2_000)

b0 >> ambi(v1.degree, lpf=1000)

b1 >> star(v1.degree, dur=PDur(7, 11)*2, amp=.5)
