import FoxDotChord

Clock.bpm = 70
chords = c[
    ['Dm7/11!8 Bb7M!7 Bb7M/5'] * 2 +
    ['Dm7M/9!4 Bb7M!2 G7/9!2'] * 2
]

c0 >> ambi(chords, dur=2, sus=2, lpf=10_000, hpf=1_000, chop=1, vib=var([0,.75], [8, 2]))

d0 >> play('|S0| (P[P-][PPP][PP]) ', dur=1, sample=P[0:8])

d1 >> play('((vXV)([*X][X*])){-[--]}(@o)(---(EW))', mix=.5, room=.5)

c1 >> star(chords, dur=PDur([6, 9], var([8, 16], 12)), hpf=800, lpf=100_000, vib=.5, amp=.7)

# --------

def nbass():
    ~c2 >> bbass(
        c['E7!4 Dm7!4 C7!4 Bb7M!2 Am7!2'],
        dur=PDur(var([9, 12], 16), 16),
        lpf=2_000,
        hpf=linvar([50, 1_000], 8),
        oct=var([4, 5], 16),
        amp=linvar([1.5, .5], .1),
    )
def interludio():
    d1 >> play('(C(ES))   ')
    Clock.future(60, base)
def base():
    base = Group(c0, c1)
    base.amp=.3
    nbass()
    Clock.future(24, lambda: base.stop())
    Clock.future(32, percu)
def percu():
    d2 >> play('pn u', amp=2)
    Clock.future(150, vira)
def vira():
    Clock.future(0, lambda: Group(c2, d2).solo())
    Clock.future(32, lambda: Group(c2, d1).solo())
    Clock.future(32*2, lambda: c2.solo())
    Clock.future(32*3, lambda: c2.solo(0))
    Clock.future(32*10, vira)
@nextBar
def pre_virada():
    Group(c1, c0).amp=linvar([.9, 0], .1)
    c1.dur=PDur(12, 8)
    Clock.future(24, interludio)

# ---

chords2 = c['B⁰ Dm7/9 F7M Gm7']
def ponte():
    Group(c2, d2, d1).stop()
    ~e3 >> ppad(chords2.arp(), dur=PDur(8, 14), oct=5)
    ~e4 >> donk(chords2.arp(), dur=PDur(8, 14), oct=5)
    ~e5 >> blip(chords2.arp(), dur=PDur(8, 14), oct=5)
    Clock.future(24, harm)
def harm():
    c3 >> pianovel(chords2, dur=PDur(3, 8), oct=4, mix=.4, room=linvar([0, .9], .1), amp=.5)
    ~e6 >> waves(e3.degree, dur=4, amp=.2).stop()
    Clock.future(36, vira)
def vira():
    Clock.future(64*3, lambda: Group(d3, c3).solo())
    Clock.future(64*6, lambda: Group(d3, c3).solo(0))
    Clock.future(64*10, vira)
@nextBar
def poppa():
    old = Group(c2, d2, d1)
    old.amp = linvar([1, 0], .05)
    old.dur = 2
    d3 >> play('X(-[---]PPV)(ET){-i}')
    Clock.future(24, ponte)
