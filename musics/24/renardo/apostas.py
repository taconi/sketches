import FoxDotChord

chords = var(
  c[
    [c*['G#m7 F7/11- F⁰'], c*('G7/4/9 A7M/9', c*['Dm7/9 Bm7/9']), c*('Cm5-/7/9', c*('Bb7M@'))],
    c*['B7/4/9 B7/4 B7 B7/11 B7/13-'],
    [c*('E7M/9 C7M/9@'), c*('Em7/9+', c*[c['C7M/6@'], c*('C7M/6@')])],
    ['D#7/9+', (3, 6, 9, 15, [39, 37]), c^['Bm7/5-@ E7/4/9', .5]],
    c^['D#7/9-@', .5],
  ],
  [4, 4, 4, 2, 2],
)

~a1 >> pluck(
  chords,
  dur=PDur(3, 8)*2,
  chop=1,
  pam=linvar([-1, 1]),
  oct=4,
  lpf=linvar([700, 5_000], 30),
  hpf=700,
).every(8, 'bubble').every(3, 'amen')

~b1 >> bounce(
  chords,
  oct=[4, 6],
  dur=PDur(12, 16)*2,
  hpf=300,
  lpf=2500,
).solo(0)

d0 >> play('([XX]S)   ', mix=.5, room=.75, chop=1, sus=1, dur=.25, hpf=600)

d1 >> play('<-   ><PS (-[--])>', lpf=var([3_000, 5_000, 10_000], 16))

d2 >> play('( |b4|)   ', lpf=var([3_000, 5_000, 10_000], 16))

d3 >> play('V{k=}V ', hpf=var([700, 10_000], 8))

a3 >> space(
  chords,
  dur=1,
  sus=.25,
  pan=linvar([1, -1], .1),
  hpf=var([200, 400], 4),
  lpf=linvar([500, 3_000], 10),
  oct=3,
)
