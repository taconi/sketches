from FoxDotChord import PChord as c

Clock.bpm = 60
har = c['D7M/9, F⁰7, Em7/4, A13']

d1 >> play('{pP}', dur=[.25, .5, .25, .5])

h1 >> razz(har, dur=2, sus=2.5, oct=var([4, 5], 8))

b1 >> sosbell(har, dur=PDur(3, 8), sus=.5, pan=[0, 1, -1, 0], hpf=linvar([600, 3_000], 100))

b2 >> star(har.every(7, 'shuffle'), dur=PDur(3, 8), lpf=4_000, amp=.5)

b3 >> pasha(har.every(7, 'shuffle'), dur=PDur([3, 5, 7], [5, 9]), lpf=4_000, amp=.5)

# --

b_all.stop()
Clock.bpm = 80


h1.amp=.4
d1 >> play('x[---]o@', dur=1)

v1 >> blip([1, 2, 4, -2], dur=PDur(3, 8), pan=[0, -1, 1, 0], sus=0.5)

e1 >> sawbass([2, 4], dur=4, sus=2)

e2 >> tribell([2, 4, 6], sus=.3, dur=PDur(3, 7), amp=linvar([.75, 1.5], .5))

d2 >> play('{!C}       ', amp=0.5, sample=var([0, 1]), dur=PDur(2, 10))
d3 >> play(':  ')
