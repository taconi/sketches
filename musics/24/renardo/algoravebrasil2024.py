# pip install renardo FoxDotChord

import FoxDotChord

# alias
f = Clock.future
g = Group
sched = lambda d: lambda fn: f(d, fn)



"""
LOOP
"""

Clock.bpm = 70
har = c[
  ['Dm7/11!8 Bb7M!7 Bb7M/5'] * 2 +
  ['Dm7M/9!4 Bb7M!2 G7/9!2'] * 2
]

~c0 >> ambi(
  har, dur=2, sus=2, lpf=10_000, hpf=1_000,
  chop=1, vib=var([0,.75], [8, 2]),
)

~d0 >> play(
  '|S0| (P[P-][PPP][PP]) ',
  dur=1,sample=P[0:8],
)

~d1 >> play(
  '((vXV)([*X][X*])){-[--]}(@o)(---(EW))',
  mix=.5, room=.5,
)

~c1 >> star(
  har, dur=PDur([6, 9], var([8, 16], 12)),
  hpf=800, lpf=100_000, bpf=900, vib=.5,
  amp=.7,
)

# --------

def nbass():
  ~c2 >> bbass(
    c['E7!4 Dm7!4 C7!4 Bb7M!2 Am7!2'],
    dur=PDur(var([9, 12], 16), 16),
    lpf=2_000,
    hpf=linvar([50, 1_000], 8),
    oct=var([4, 5], 16),
    amp=linvar([1.5, .5], .1),
  )
def interludio():
  d1 >> play('(C(ES))   ')
  f(60, base)
def base():
  base = g(c0, c1)
  base.amp=.3
  nbass()
  f(24, lambda: base.stop())
  f(32, percu)
def percu():
  ~d2 >> play('pn u', amp=2)
  f(150, vira)
def vira():
  f(0, lambda: g(c2, d2).solo())
  f(32, lambda: g(c2, d1).solo())
  f(32*2, lambda: c2.solo())
  f(32*3, lambda: c2.solo(0))
  f(32*10, vira)  # loop
@next_bar
def pre_virada():
  g(c1, c0).amp=linvar([.9, 0], .1)
  c1.dur=PDur(12, 8)
  f(24, interludio)

# ---

har = c['B⁰ Dm7/9 F7M Gm7']
def ponte():
  d1.stop()
  ~e3 >> ppad(har.arp(), dur=PDur(8, 14), oct=5)
  ~e4 >> donk(har.arp(), dur=PDur(8, 14), oct=5)
  ~e5 >> blip(har.arp(), dur=PDur(8, 14), oct=5)
  f(24, harm)
def harm():
  ~c2 >> pianovel(
    har,dur=PDur(3,8),oct=4,mix=.4,
    room=linvar([0, .9], .1), amp=.5,
  )
  ~e6 >> space(e3.degree, dur=4, amp=.5)
  f(36, vira)
def vira():
  f(64*3, lambda: g(d2, c2).solo())
  f(64*6, lambda: g(d2, c2).solo(0))
  f(64*10, vira)
@nextBar
def poppa():
  old = g(c2, d2, d1)
  old.amp = linvar([1, 0], .05)
  old.dur = 2
  ~d2 >> play('X(-[---]PPV)(ET){-i}')
  f(24, ponte)




Clock.clear()













"""
CUCA
"""

Clock.bpm = 80

@sched(1)
def _():
  ~k1 >> keys(
    c[c*('Bm7/9', ['Em7/9 Bb7M/9']), 'Am7/9'],
    dur=4,hpf=800,
  )
@sched(16)
def _():
  ~g1 >> bassguitar(
    c[
      c*(c*('Bm7@'), [c*('Em7@'), c*('Bb7M@')]),
      c*('Am7/2@'),
    ],
    dur=4,amp=.5,
  )
@sched(50)
def beat():
  ~b1 >> play('<(xc)s><(-(*|!{35}|)) >')
  ~b2 >> play('(SE)P',dur=4,room=.5,mix=.5)
@sched(112)
def pausa():
  b_all.stop()
  k1.lpf = linvar([800, 1000], 1)
  g1.hpf = 400
@sched(138)
def volta():
  # g1.solo(0)
  k1.lpf = linvar([800, 1000], 1)
  g1.hpf = 700
  ~b1 >> play('<(XC)S><(_(8V)) >')
  ~b2 >> play('(se)p', dur=4, room=.5, mix=.5)
  ~b3 >> play(
    '<[VV] P ><-hs(hhhhh[hh])>', sample=6
  )
@sched(258)
def virada():
  ~g2 >> latoo(amp=0.25)  # ruido
  g1.hpf = 400
  g_all.solo()
  f(25, lambda: g1.stop())
chords = c[
  c[
    c*('Am7/5-/2/4/6/9@'),
    c*('Am7/2@'),
    c*('Am Am7M'),
  ], # 1e2
  c*('Bm7@'), # 3
  c*('E7@')   # 4
]
#      1e2  3  4
dur = [  4, 2, 2]
@sched(315)
def _():
  ~b1 >> laserbeam(chords, dur=dur, amp=.5)
@sched(375)
def _():
  b2 >> bassguitar(
    c[c*('Am7/2@'), c*(c*('Bm7@'), c*('E7@'))],
    dur=4,
  )
@sched(440)
def _():
  b3 >> keys(
    var(
      c[
        ['Am7    F7M/9'],
        ['Bm7/5  Gm7/9'],
        ['E7/13- C7/9'],
      ],
      dur,
    ),
    dur=dur,
  )
# f(500, Clock.clear)


next_bar(Clock.clear)








"""
RODO
"""

Clock.bpm = 180


har = c['Dm7 C7/9 Bb7/9 A7']

~x1 >> keys(
  var(har,8),
  dur=var([(PDur(7,12)*2,PDur(3,5))],8),
)

~d1 >> play('--- ')
~d2 >> play('V   ')
~d3 >> play('+xo+')
~d4 >> play('({!C}(ES))   ', room=1,hpf=300)

~x2 >> orient(
  x1.degree,dur=PDur(7,9)*2,amp=.7,oct=4,
  lpf=7_000, bpf=P[700,1_000,1_500,2_000],
)


x1.dur = PDur(7, 9) * 2

d4 >> play('(C(ES))   ')

g(d1, d2).stop()



har2 = c['F#6, Dm7/9, Bm5/7']

x1.amp = .7
x2.stop()
~x3 >> swell(
  var(c['F#6, Dm7/9, Bm5/7'], [4, 8, 4]),
  dur=PDur(5, 9)*2, oct=var([4, 5], 16),
)

~d5 >> play('pn u', amp=2)
~d6 >> play(
  'XV',sample=P[:8],amp=.5,pan=linvar([-1,1],4)
)

d4.stop()

f(4, lambda: g(x3,d3).solo())
f(70, lambda: g(x3,d3).solo(0))
f(70, Clock.clear)










"""
VULGO
"""

Clock.bpm = 120

har = c['Am9/7 Gm6/4 F7M/2 E7/4']

~c1 >> filthysaw(
  var(har, [8]), dur=PDur(3, 8)*2,bpf=1000
)

~d1 >> play('--- ')

~d2 >> play('S   ', dur=2)

~d3 >> play('O   ', dur=8)

~d4 >> play('ry')

~b1 >> space(
  har.i,
  dur=var(
    [PDur(3,[4,8]),c1.dur,PDur([7,5],9)],[4,8]
  ),
)

~b2 >> varsaw(
  c['C6/13!8 A#!8 Fm13/5-!8 Bm5/11!6 Gm7!2'],
  dur=PDur(3, 6)*2, amp=.8, oct=4, bpf=1000,
)

d4 >> play('V ')
d5 >> play(' -o-')

gp1=g(c1,d1,d4,d4,b1)

@next_bar
def _():
  gp1.crush=[12,15,8,5]
  gp1.solo()
@sched(32)
def _():
  gp1.solo(0)
  b2.stop()

g(c1,d2,d3).stop()

g(b1,d2,d3,d4,d5).stop()










"""
PAUSA PRA PITAR
"""

Clock.bpm = 40
i = g(b1, d2)
m = g(a1, d2)
har = c[
  c*(c*('A7M!2'),c*('A7M/6!2@ A7M/9!2@ A7M/5-!2@')),
  c*(c*('C7M!2'),c*('C7M/9!2@')),
  c*(c*('G7M!4'),c^('G7M/9/6!4@', .5)),
]
def baixo():
  ~b1 >> acidbass(
    har,amplify=2,amp=.75,
    sus=var([.75, .25], 64),dur=PDur(12, 16)*2,
  )
def harmonia():
  ~a1 >> sinepad(
    har,amp=var([.5,.75], 64),oct=6,hpf=800,bpf=500,
    lpf=sinvar([3_000, 15_000], 16),
    dur=PDur(12, 8)*4, tremolo=linvar([2, 0], .5),
  )
def dale():
  m.solo(0)
  ~d1 >> play('<X-o-><V{p|S3|}>')
def ritmo():
  ~d2 >> play('[ ---]')
  f(4, m.solo)
  f(8, dale)
def quebra():
  i.solo()
  f(8, lambda: i.solo(0))
@next_bar
def start():
  baixo()
  f(32, harmonia)
  f(32*2, ritmo)
  f(32*3, quebra)
  f(32*4, quebra)
  f(32*5, Clock.clear)


Clock.clear()














"""
SAILA
"""

Clock.bpm = 50
har = c['D7M/9 F⁰7 Em7/4 A13']

~d1 >> play('{pP}', dur=[.25, .5, .25, .5])

~h1 >> razz(har, dur=2, sus=2.5, oct=var([4, 5], 8))

~b1 >> bellmod(
  har, dur=PDur(3, 8), sus=.5, pan=[0, 1, -1, 0],
  hpf=linvar([600, 3_000], 100), lpf=2000,amp=.35
)

~b2 >> star(
  har.every(7, 'shuffle'), dur=PDur(3, 8),amp=.5,
  lpf=4_000, bpf=1000,
)


g(b1, b2).bpf = 2000
~b3 >> pasha(
  har.every(7, 'shuffle'), lpf=4_000, bpf=1000,
  amp=.5, dur=PDur([3, 5, 7], [5, 9]),
)

# --

b_all.stop()
Clock.bpm = 80


h1.amp=.5
~d1 >> play('x[---]o@', dur=1)

~v1 >> blip(
  [1, 2, 4, -2], dur=PDur(3, 8),
  pan=[0, -1, 1, 0], sus=0.5,
)

~e1 >> sawbass(
  [2, 4], dur=4, sus=2,crush=[4,6,8,10,12]
)

~e2 >> faim(
  [2, 4, 6], sus=.3, dur=PDur(3, 7), chop=[1,3]
).every(3,'stutter', dur=4)


~d2 >> play(
  '{!C}       ', amp=0.5, sample=var([0, 1]),
  dur=PDur(2, 10),
)
~d3 >> play(':  ')



b_all.stop()

g(v1,e1,e2,d2,d3).stop()


g(d1,d2,d3,h1).stop()










"""
ILHA

inspirada em  "A Ilha" de Djavan
"""

Clock.bpm = 70

har = c[
  'Am7M/9!2 F7/11+!2 Bm7/5-!2 E7/13-!2 ' * 3 +
  'Am7M/9!2 F7/11+!2, Dm7/9!2 G7/4/9 F7/11+ ' +
  'Em7/b-!4 A7/13-!2, Eb7/9!2 ' +
  'Dm7/9!4 G7/4/9!2, Db7/9+!2 ' +
  'C7M/5+!2 C7M!2, F7/11+!4 ' +
  'Bm7/5-!4 E7/13-!4 ' +
  'Am7M/9!2 F7/11+!2 Bm7/5-!2 E7/13-!2'
]
~v1 >> pasha(
  har, dur=PDur(3, 8),
  lpf=linvar([500, 1000, 500], 50),crush=4,
)

~d1 >> play('P', room=.7, mix=.5)

~d2 >> play(
  'S({!b} )', room=.7, mix=.5,
  lpf=linvar([1_000, 5_000], [50, 100]),
)

~d3 >> play('--')

~d4 >> play('VpOe', hpf=2_000)

~d5 >> play('X', dur=[2, 2, 4],room=1, lpf=10_000,hpr=400,lpr=10)

~b0 >> ambi(v1.degree, lpf=1000,room=1,mix=1,echo=2,bpf=2000,mpf=1000,hpf=200)



i = g(v1,d1,d2,d3,d5)
i.solo()

~b1 >> star(
  v1.degree, dur=PDur(7, 11)*2, amp=.25,lpf=4000,
  hpf=500,mpf=[7_000, 15_000],
)
~d6 >> play('V.')

f(4, lambda: i.solo())
f(4*12, lambda: i.solo(0))



g(b0,b1,d4,d6).stop()
i.stop()





"""
PROMESSAS
"""

Clock.bpm = 120

chords = c['A7M!4 C7M!4 G7M!8']

~a1 >> epiano(
  chords,
  hpf=800,
  lpf=sinvar([3_000, 15_000], 16),
  amp=.75,
)
~a2 >> mpluck(
  chords, pan=linvar([1, -1], .1), amp=.5,
  chop=1, room=.7, mix=.5, hpf=500,
  lpf=sinvar([3_000, 15_000], 16),
)
~a3 >> bchaos(
  chords.values.arp(), hpf=500, chop=[3, 6],
  lpf=sinvar([3_000, 15_000], 16),
)
~b1 >> play(
  '<v V ><(-|P2|)(io)>', chop=.75,
  sus=[1, .5, .25, 2, .01],
  echo=var([.25, .5], 16),
)

~c1 >> wobblebass(
  chords.values.deg('v,i,iii'),
  dur=chords.dur,
  oct=4,
  hpf=800,
  amp=1.25,
)
~c2 >> sine(chords.values.deg('v,i,iii'), dur=chords.dur)

~b2 >> play(P^['{pP}k', .25], dur=P[3/6])
g(a1, b1, b2).amp = linvar([1.15, .25], [.1, .01, .01])

~d1 >> space(
  chords,
  oct=var([4, 5], 16),
  lpf=linvar([5_000, 15_000], 16/4),
  hpf=700
)
~d2 >> ambi(
  chords, hpf=700, amp=.5,
  oct=var([4, 5], 16),
  lpf=linvar([5_000, 15_000], 16/4),
)

@next_bar
def respira():
  g(a1, d1).solo()
  f(32, lambda: g(a1, b2, d1).solo())
  f(32*2, lambda: g(a1, b2, d2, d1).solo())
  f(32*4, lambda: g(a1, b2, d2, d1, a2).solo())
  f(32*6, lambda: g(a1, d1).solo())
  f(32*7, lambda: g(a1, d1).solo(0))

@next_bar
def fim():
  g(a1, b2, d1).solo()
  f(32, Clock.clear)


"""
JUST

inspirada em "Just The Two Of Us" by Bill Withers
"""

Clock.bpm = 160

def base():
  a1 >> mhping(
    c[
      'Db7M/9',
      'C7/9',
      ['Fm7/9'] * 4                 + [c*('Fm7 Em7'), 'Fm7/9+'] * 2,
      [c*('Ebm7/9 Ab7/9'), 'Fm7/9'] * 2 + [c*('Ebm7 Ab7'), 'Fm7/9'] * 2,
    ],
    dur=4,
    sus=2,
  )
def baixo():
  b1 >> ebass(
    [
      P^[.5, 1.5, .5, .5], 0, 2, P^[3, 4, 3, .5], 1.5, -2.5,
      P^[.5, 1.5, .5, .5], 0, 2, 3
    ],
    dur=[
      4, 2, 2,
      4, 2, 2,
      4, 2, 2,
      8,
    ],
  )
@next_bar
def dale():
  f(4, lambda: x1 >> play('vgo&', dur=1))
  f(40, baixo)
  f(104, base)

b1.amp=.75
b1.bpf=1_500


Master().lpf=4000


Clock.bpm = var([160, 190, 200, 220],[4*4, 4*4, 8*4, 10*4])

# -------

g(a1,b1).stop()
x1.stop()









"""
BABY STEPS

inspirada em "Giant Steps" by John Coltrane
"""

Clock.bpm = 90

~c2 >> play('{d  }', dur=[1/16, 1/8, 1/32], amp=.09)

chords = c[
  'B7M D7 G7M Bb7!.7 Eb7M!2.3 Am7!.7 D7!1.3',
  'G7M B7 Eb7M F#7!.7 B7M!2.3 Fm7 Bb7!.7',
  'Eb7M!2.3 Am7 D7!.7 G7M!2.3 C#m7 F#7!.7',
  'B7M!2.3 Fm7 B7M!.7 Eb7M!2.3 C#m7!.7 F#7!1.3',
]

~l2 >> space(
  chords.values.deg('i, vii, iii, v'),
  dur=chords.dur, mix=1, room=1, echo=1,
  hpf=700, lpf=1_000, amp=.4,
)  # from marte


c1 >> play(
  '<-WE-><N-& >',
  dur=[.25,1,.5,2,.25,.25],lpf=4_000,amp=.5,
)

~s1 >> piano(
  chords, dur=chords.dur,
  pan=[-1, 0, 1, 0], crop=20, amp=1,
  lpf=4_000,lpr=4,hpf=500,hpr=1
)


f(31, lambda: g(c1,s1,l2).solo())
f(31*2, lambda: g(c1,s1,l2).solo(0))


~l1 >> bellmod(
  chords.values.i,
  dur=chords.dur,
  hpf=900,
)#.follow(s1) + (2, 3)

~b1 >> play('=----{-@}--')
~b2 >> play('Vkokv([kk]k[ k]])(@v)k')

b1.stop()

~l1 >> bphase(
  chords, amp=.5, dur=PDur(3, 8),
  oct=var([5, 6], 16),
  lpf=4_000, lpr=2, hpf=400,
)


f(31, lambda: g(c1,s1,l2).solo())
f(31*2, lambda: g(c1,s1,l2).solo(0))


# -------

f(32, lambda: g(l2,c1,s1,l1).stop())
f(32*2, lambda: g(c2,l2).stop())







"""
CONTRATOS

inspirada em "Apostas e Contratos" by Ashira
"""

Clock.bpm = 110

har = var(c['G#m7, B7/4/9, E7M/9, D#7/9+, D#7/9-'], [4, 4, 4, 2, 2])
a4 >> pluck(
  har, dur=PDur(3, 8)*2, pam=linvar([-1, 1]),
  oct=4, lpf=linvar([700, 5_000], 30),
)

d1 >> play(
  '<-   ><PS (-[--])>',
  lpf=var([3_000, 5_000, 10_000], 16),
)

d2 >> play(
  '( |!5|)   ',
  lpf=var([3_000, 5_000, 10_000], 16),
)

d2.stop()

a3 >> space(har, dur=1, hpf=var([200, 400], 4), lpf=linvar([500, 3_000], 10),amp=.4)

m1 >> dab(
      [4.5, 6, 7.5, 8.5, 11.5, 10.5, 9, 8.5,   8.5,  4.5, 6, 0],
  dur=[1,   1, 1,   1,   1,     1,   1, .75,   1.25, 1,   1, rest(5)],
  oct=6,
  sus=1.25,
  lpf=linvar([300, 3_000], 100),
  hpf=linvar([100, 900], 100),
  amp=.4
)

g(m1, a3).stop()

chords = var(
  c[
    [c*['G#m7 F7/11- F⁰'], c*('G7/4/9 A7M/9', c*['Dm7/9 Bm7/9']), c*('Cm5-/7/9', c*('Bb7M@'))],
    c*['B7/4/9 B7/4 B7 B7/11 B7/13-'],
    [c*('E7M/9 C7M/9@'), c*('Em7/9+', c*[c['C7M/6@'], c*('C7M/6@')])],
    ['D#7/9+', (3, 6, 9, 15, [39, 37]), c^['Bm7/5-@ E7/4/9', .5]],
    c^['D#7/9-@', .5],
  ],
  [4, 4, 4, 2, 2],
)

~a4 >> pluck(
  chords,
  dur=PDur(3, 8)*2,
  chop=1,
  oct=4,
  lpf=linvar([700, 5_000], 30),
  amp=.6,
  # hpf=700,
)#.every(8, 'bubble').every(3, 'amen')


m = g(d1,d2,d3,d4,a4)
f(16, lambda:m.solo())
f(16*2, lambda:m.solo(0))

~b1 >> bounce(
  chords, hpf=300, lpf=2500,
  oct=[4, 6],
  dur=PDur(12, 16)*2,
)

a3 >> space(
  chords, dur=1, sus=.25, oct=3,
  pan=linvar([1, -1], .1),
  hpf=var([200, 400], 4),
  lpf=linvar([500, 3_000], 10),
)

m.solo()

g(a4,a3).stop()


"""
 1. LOOP
 2. CUCA
 3. RODO
 4. VULGO
 5. PAUSA PRA PITAR
 6. SAILA (mudar/melhorar segunda parte)
 7. ILHA (falta algo)
 8. PROMESSAS (podia vir depois de SAILA)
 9. JUST (ta meio vazio, precisa de volume e movimento)
10. BABY STEPS (foi feito com renardo==1.0.0,dev-13 e alguns synths não funcionaram no 0.9.13)
11. CONTRATOS
"""
