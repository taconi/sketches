"""
"Just The Two Of Us" by Bill Withers
"""

import FoxDotChord

# Tom: Fm
# FoxDotChord por enquanto só tem suporte para
# Root.default = 'F'       # 'C'
# Scale.default = 'minor'  # 'major' | 'chromatic'
Root.default = 'C'
Scale.default = 'major'
Clock.bpm = 160

def base():
    a1 >> mhping(
        c[
            'Db7M/9',
            'C7/9',
            ['Fm7/9'] * 4                 + [c*('Fm7 Em7'), 'Fm7/9+'] * 2,
            [c*('Ebm7/9 Ab7/9'), 'Fm7/9'] * 2 + [c*('Ebm7 Ab7'), 'Fm7/9'] * 2,
        ],
        dur=4,
        sus=2,
    )
def baixo():
    b1 >> ebass(
        [
            P^[.5, 1.5, .5, .5], 0, 2, P^[3, 4, 3, .5], 1.5, -2.5,
            P^[.5, 1.5, .5, .5], 0, 2, 3
        ],
        dur=[
            4, 2, 2,
            4, 2, 2,
            4, 2, 2,
            8,
        ],
    )
@next_bar
def dale():
    Clock.future(4, lambda: x1 >> play('vgo&', dur=1))
    Clock.future(40, baixo)
    Clock.future(104, base)

next_bar(Clock.clear)
