import FoxDotChord
Clock.bpm = 80
def sched(delay: int):
    def inner(func):
        Clock.future(delay, func)
    return inner
@sched(1)
def _():
    ~k1 >> keys(c[c*('Bm7/9', ['Em7/9 Bb7M/9']), 'Am7/9'], dur=4, hpf=800)
@sched(16)
def _():
    ~g1 >> bassguitar(
        c[
            c*(c*('Bm7@'), [c*('Em7@'), c*('Bb7M@')]),
            c*('Am7/2@'),
        ],
        dur=4,
    )
@sched(50)
def beat():
    ~b1 >> play('<(xc)s><(-(*|!{35}|)) >')
    ~b2 >> play('(SE)P', dur=4, room=.5, mix=.5)
@sched(112)
def pausa():
    b_all.stop()
    k1.lpf = linvar([800, 1000], 1)
    g1.hpf = 400
@sched(138)
def volta():
    # g1.solo(0)
    k1.lpf = linvar([800, 1000], 1)
    g1.hpf = 700
    ~b1 >> play('<(XC)S><(_(8V)) >')
    ~b2 >> play('(se)p', dur=4, room=.5, mix=.5)
    ~b3 >> play('<[VV] P ><-hs(hhhhh[hh])>', sample=6)
@sched(258)
def virada():
    g2 >> latoo(amp=0.25)  # ruido
    g1.hpf = 400
    g_all.solo()
    Clock.future(25, lambda: g1.stop())
# ---
chords = c[
    c[c*('Am7/5-/2/4/6/9@'), c*('Am7/2@'), c*('Am Am7M')], # 1e2
    c*('Bm7@'), # 3
    c*('E7@')   # 4
]
#      1e2  3  4
dur = [  4, 2, 2]
@sched(315)
def _():
    ~b1 >> laserbeam(
        chords,
        dur=dur,
        amp=.5
    )
@sched(375)
def _():
    b2 >> bassguitar(c[c*('Am7/2@'), c*(c*('Bm7@'), c*('E7@'))], dur=4)
@sched(440)
def _():
    b3 >> keys(
        var(
            c[
                ['Am7    F7M/9'],
                ['Bm7/5  Gm7/9'],
                ['E7/13- C7/9'],
            ],
            dur,
        ),
        dur=dur,
    )
Clock.future(600, Clock.clear)
