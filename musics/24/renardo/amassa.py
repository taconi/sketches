import FoxDotChord

# Alias

f = Clock.future
g = Group
def sched(dur):
  """
  @sched(8)
  def _():
    s1 >> play('sd')
  """
  def inner(func):
    f(dur, func)
    return func
  return inner

~d1 >> play('({XV}(Oo))(-[---])')

~d2 >> play('(TTTTTTTTTTTTT[l\]) ',dur=.25)

har= c[
  'C#7/9+!4 Bm7/b5!8 Bbm6!2 Eb7/9!2'
]
~b1 >> bass(
  har.values.i,
  dur=har.dur,
  amp=.4,
  oct=4,
)
~h1 >> epiano(
  har,
  dur=PDur(3,8),
  amp=.4
)

gp0 = g(d1,b1,h1)

@sched(8)
def _():
  gp0.solo()
@sched(8*4)
def _():
  gp0.solo(0)

d2.stop()
