import FoxDotChord

Clock.bpm = 120

~a1 >> play(
    '! <H><!> ',
    amp=.75,
    chop=var([1, 0], [32, 8]),
    decay=.5,
    room=.2,
    mix=.2,
)

~b1 >> play(
    '|V2| ',
    dur=4,
    vib=6,
    vibdepth=.5,
    echo=.5,
    echodelay=.25,
    pan=var([-1, 0, 1, 0], 8),
    hpr=sinvar(30, 90),
    lpf=linvar([200, 20_000], 16),
)

~c1 >> play(
    '|e2|',
    dur=PDur([9,8], [15, 8, 13, 14]),
    hpf=linvar([2_000, 15_000], 16),
)

h = c[
    'C7M/5+ F7/11+ Am7M/9',
    ['G7/6 Db7/9+'],
]

~k1 >> ecello(
    h,
    dur=8,
    vib=16,
    vibdepth=var([.05, .5], [64, 6]),
    amp=.5,
    oct=[6, 7],
)

~v1 >> ebass(
    [z.tonic for z in h],
    dur=8,
    sus=10,
    oct=6,
    slide=-1,
    slidedelay=1,
    spin=8,
    stutter=1,
    mix=.75,
    room=.5
)

# respiro
Group(b1, c1, v1).solo()
Clock.future(40, lambda: Group(b1, c1, v1).solo(0))
Clock.future(40*2, lambda: Group(b1, v1).solo())
Clock.future(40*4, lambda: Group(b1, v1).solo(0))


k1.vibdepth = 0
k1.oct=P[6, 6, P[6, 7], 6, 6, 6, 7, 6]
x = Group(b1, v1, k1)
x.amp=.25
x.solo(0)

~f1 >> flute(
    var([z.tonic for z in h], 8),
    dur=PDur(7, 12)*2,
    oct=4,
    amp=2,
    pan=var([0, 1, 0, -1], 8),
)

~m1 >> marimba(
    var([z.tonic for z in h], 8),
    dur=PDur(7, 12)*2,
    pan=var([1, 0, -1, 0], 8),
)

~f2 >> flute(
    var([z.third for z in h], 8),
    dur=PDur(8, var([15, 14], 2)),
    oct=4,
    amp=2,
    pan=var([0, -1, 0, 1], 8),
)

~m2 >> donk(
    [z.third for z in h],
    dur=PDur(8, var([15, 14], 2)),
    oct=6,
    pan=var([-1, 0, 1, 0], 8),
)

~f3 >> flute(
    [z.dominant for z in h],
    dur=PDur(12, var([16, 14], 4)),
    oct=7,
    pan=var([0, 1, 0, -1], 8),
)

~m3 >> moogpluck(
    var([z.dominant for z in h], 8),
    dur=PDur(3, 8)/2,
    pan=var([1, 0, -1, 0], 8),
    amp=1.5,
    lpf=linvar([5_000, 15_000], 8),
).every(3, 'bubble')

~f4 >> flute(
    var([z.maj for z in h], 8),
    dur=PDur(12, 16),
    pan=var([0, 1, 0, -1], 8),
    oct=4,
    amp=4,
    spack=1,
).every(4, 'reverse')

~m4 >> karp(
    [z.maj for z in h],
    pan=var([-1, 0, 1, 0], 8),
    dur=PDur(6, 8),
    spin=16,
    chop=8,
    oct=4,
    amp=1,
).every(8, 'shuffle')

# bye
Group(b1, c1, v1, f_all).solo()
Clock.future(40, Clock.clear)
