from FoxDotChord import PChord as c

var.chords = c['Am9/7, Gm6/4, F7M/2, E7/4']
c1 >> piano(var(var.chords, [8]), dur=PDur(3, 8)*2)

d1 >> play('--- ')

d2 >> play('S   ', dur=2)
d3 >> play('O   ', dur=8)

d4 >> play('ry')

b1 >> space(
    [c.tonic for c in var.chords],
    dur=var([PDur(3, [4, 8]), c1.dur, PDur([7, 5], 9)], [4, 8]),
)


c1.stop()



b2 >> varsaw(
    var(c['C6/13, A#9, Fm13/5, Bm5/11, Gm7'], [8, 8, 8, 6, 2]),
    amp=.8,
    dur=PDur(3, 6)*2,
    oct=4
)
d4 >> play('V ')
d5 >> play(' -o-')
