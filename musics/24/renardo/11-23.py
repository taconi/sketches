# pip install FoxDotChord
import FoxDotChord

d1 >> play('(xo)-', samples=3)
d2 >> play('V;', room=.5, mix=.5)


h = c[
  'E7M/9!5 Dm7/9+'
]
~m1 >> keys(
  h, dur=PDur(3,8)*4,sus=2,lpf=2000,
  amp=.75,
)

~b1 >> ebass(
  m1.pitch, dur=4,amp=.75,hpf=700,
  crush=2,room=.75
)


b1.stop()
m1.stop()



h = c[
  'Am9!2 G7M/9!2 F7/11+!2',
  c^['E7/13+ E7/13-', .75],
]
~m1 >> epiano(
  h, dur=PDur(4,[11,12]),crush=4,
)

d1 >> play('<ST>h',room=.5,hpf=200)
d2 >> play(
  'VV[VV]     ( [VV])    ',
  sample=var([3, 2, 1], [2,3,5]),
)


m2 >> space(h, amp=.4, sus=1.5)
m3 >> ambi(h, amp=.4, sus=1.5, crush=2)

m_all.stop() # POR HOJE É SÓ
d_all.stop() # FALOU VALEU

