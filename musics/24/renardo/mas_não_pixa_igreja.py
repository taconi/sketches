"""
@title Mas não pixa igreja
@by taconi
@license CC
@date 1/12/2024

pip install 'renardo==1.0.0.dev13'
pip install FoxDotChord   # acordes cifrados
pip install 'git+https://codeberg.org/taconi/renardo-shabda.git'  # text-to-speech e random samples free
"""

import FoxDotChord

Clock.bpm = 120
Root.default='C'
Scale.default='major'

~d1 >> play('(xo)p',lpf=2000,room=.5,mix=.5)

har = c[
  'C7M F7M Em7 G7'
]

~m1 >> keys(
  har,
  dur=PDur(3,8)*2,
  lpf=1500,
  hpf=600,
)

~b1 >> bassguitar(
  har.deg('i,vii,iii,v'),
  dur=PDur(3,11)*2,
  chop=5,
  sus=2,
  att=.4,
  hpf=300,
)

~s1 >> space(
  har,
  dur=8,
  echo=2,
  slide=1,
  slidelay=2,
  amp=var([0,.5], [4,7])
)

~s2 >> ambi(har.deg('i,v,vii'), dur=PDur(5,14),chop=4,att=.5)

s1.stop()

d2 >> play('V.', room=.5,mix=.25)

Group(s2, d2,d1).solo()

s2.stop()

har = c['C7M!4 B7M!4 G7M!8']

s1 >> arpy(
  har,
  dur=PDur(3,8),
  lpf=2500,
  hpf=600,
  hpr=var([.4, 1], [8, 6, 4]),
).every(3, 'stutter', 4, oct=4, pan=[-1,0,1,0])

~b1 >> ebass(
  har.values.i,
  dur=PDur(3,8),
  chop=3,
).every(5, 'reverse')

~d3 >> play('<X(-[---]I[IK][Il][--]u|S1|k)><k*><Pc>')


# salva novos sons na pasta _loop_/
from renardo_shabda import samples, speech

# busca samples aleatórios no freesound.org
samples('superlativo:10,caos:10')

l1 >> loop('superlativo', [0, 3, 1, 2], dur=PDur(3,11), sample=3)
l2 >> loop('caos', dur=8, sample=1, amp=var([0, 1], [16, 4]))

# transforma o texto em um sample (text-to-speech)
speech('fala_pra_caralho,mas_nao_pixa_igreja', 'pt-BR')

v1 >> loop('fala_pra_caralho', dur=8, amp=var([0,1], [2, 8]))
v2 >> loop('mas_nao_pixa_igreja', dur=4, amp=var([0,1], [4, 6]))


from renardo_lib.Buffers import GranularSynthDef

granular = GranularSynthDef()

samples('siren:2,poetry:10,caos:10')

l3 >> loop('siren', dur=PDur(3,11)+PDur(3,8)*2, sample=1)
l4 >> loop('caos', dur=10, sample=7,amp=.5)
l5 >> granular('poetry', 2, dur=20, sample=1,amp=.15)
l6 >> granular(
  'poetry', dur=PDur(3,11), sample=8, amp=var([0,.75], [60, 20])
)
l7 >> loop('poetry', dur=20, sample=9,amp=var([0,1], [30, 5]))

Clock.future(16*2, lambda: Group(b1, s1, l3).solo())
Clock.future(16*6, lambda: Group(b1, s1, l3).solo(0))


# fim
Clock.future(16, lambda: Group(b1, s1, l3).solo())
Clock.future(16*4, Clock.clear)
