import FoxDotChord
from renardo_lib.present import *

chords = c['A7M!4 C7M!4 G7M!8']

a1 >> rhpiano(chords, hpf=500, lpf=sinvar([3_000, 15_000], 16))
~a2 >> xylophone(chords, pan=linvar([1, -1], .1), chop=1, room=.7, mix=.5, hpf=500, lpf=sinvar([3_000, 15_000], 16))
a3 >> bchaos(chords.values.arp(), hpf=500, lpf=sinvar([3_000, 15_000], 16))

b1 >> play('<v V ><(-|P2|)(io)>', chop=.75, sus=[1, .5, .25, 2, .01], echo=var([.25, .5], 16))

~c1 >> wobblebass(chords.values.deg('v,i,iii'), dur=chords.dur, oct=4, hpf=800, amp=1.25)
~c2 >> sine(chords.values.deg('v,i,iii'), dur=chords.dur)

b2 >> play(P^['{pP}k', .25], dur=P[3/6])
Group(a1, b1, b2).amp = linvar([1.25, .25], [.1, .01, .01])

d1 >> space(chords, oct=var([4, 5], 16), lpf=linvar([5_000, 15_000], 16/4), hpf=700)
d2 >> ambi(chords, oct=var([4, 5], 16), lpf=linvar([5_000, 15_000], 16/4), hpf=700, amp=.5)

@nextBar
def respira():
  Group(a1, d1).solo()
  Clock.future(32, lambda: Group(a1, b2, d1).solo())
  Clock.future(32*2, lambda: Group(a1, b2, d2, d1).solo())
  Clock.future(32*4, lambda: Group(a1, b2, d2, d1, a2).solo())
  Clock.future(32*6, lambda: Group(a1, d1).solo())
  Clock.future(32*7, lambda: Group(a1, d1).solo(0))

@nextBar
def fim():
  Group(a1, b2, d1).solo()
  Clock.future(32, Clock.clear)
