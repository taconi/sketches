from FoxDotChord import PChord as c

Clock.bpm = 110

har = var(c['G#m7, B7/4/9, E7M/9, D#7/9+, D#7/9-'], [4, 4, 4, 2, 2])
a1 >> pluck(har, dur=PDur(3, 8)*2, pam=linvar([-1, 1]), oct=4, lpf=linvar([700, 5_000], 30))

d1 >> play('<-   ><PS (-[--])>', lpf=var([3_000, 5_000, 10_000], 16))

d2 >> play('( |!5|)   ', lpf=var([3_000, 5_000, 10_000], 16))

d3 >> play('V{k=}V ', hpf=var([700, 10_000], 8))

a3 >> space(har, dur=1, hpf=var([200, 400], 4), lpf=linvar([500, 3_000], 10))

m1 >> dab(
      [4.5, 6, 7.5, 8.5, 11.5, 10.5, 9, 8.5,   8.5,  4.5, 6, 0],
  dur=[1,   1, 1,   1,   1,     1,   1, .75,   1.25, 1,   1, rest(5)],
  oct=6,
  sus=1.25,
  lpf=linvar([300, 3_000], 100),
  hpf=linvar([100, 900], 100),
)

Group(m1, a3).stop()
