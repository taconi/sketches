from FoxDotChord import PChord as C
Scale.default = 'chromatic'

c = C['Gsus7/-9, F6/b5, Dm7/+9, C6/4/9']

o1 >> organ2(
    var(c, [4, 4, 2, 2]),
    dur=PDur(3, 9) * 6,
    room=.2,
    mix=.4,
    pan=linvar(0, 1)
)

x1 >> play(P['{ydy}  '].amen(), dur=PDur(2, 5)*4)
x2 >> play(P['(fkl){!|W1|}'].every(3, 'stutter').amen(), dur=PDur(3,8)*4)

x3 >> play('((X[Xo])[---])', dur=PDur(4, 12)*2)


o2 >> sosbell(
    c.every(4, 'bubble').every(3, 'stutter'),
    dur=PDur(3, 9) * 4,
    sus=2,
    slide=.5,
    oct=4
)

x3.stop()
b1 >> bassguitar(
    c.every(8, 'stutter'),
    dur=PDur(2, 5)*3,
)

o1.stop()
b2 >> tremsynth(
    c.every(8, 'bubble'),
    dur=PDur(2, 5)*3
)

