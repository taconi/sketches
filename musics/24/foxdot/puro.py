from FoxDotChord import PChord as p

Scale.default = 'chromatic'

har = var(p['F#7M/6, Cm7/5b, E#4, D#m7, C#7M/9'], [4, 2, 2, 4, 4])
h1 >> keys(har, dur=PDur(7, 8))

x2 >> play('V   ')
x3 >> play('@   ', dur=2)
x4 >> play('|!4|   ', dur=4, amp=.5)

s1 >> space(har, amp=.5, dur=4)

b1 >> bassguitar(har, amp=.5, dur=PDur(var([3, 5, 7], 4), 8))
