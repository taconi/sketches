from FoxDotChord import PChord as c

c1 >> keys(var(c['C7M, Ebm7/9, Abm6, Bb7'], 8), dur=PDur(3, 8)*2)

c2 >> space(c1.degree, dur=PDur(7, 11))

d1 >> play('--X(t(as))', mix=.5, room=1, amp=1.5)

d2 >> play('(q[wV])')

d7 >> play('{|!{236}|,[Pp]}')

b1 >> bassguitar(var([1, 3, 5, 2], 4), dur=PDur(7, 11), amp=2)

b2 >> bug([0, 2, 1, 3], dur=4, amp=2, sus=2, oct=6)
