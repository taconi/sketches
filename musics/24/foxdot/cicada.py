from FoxDotChord import PChord as C
Scale.default = 'chromatic'


x1 >> pluck(
    var(C['Eb7M/6, Dbm7/-5, Cm7/+9, Bbm7/4/9'], [2, 4, 4, 2]),
    dur=PDur(3,9) * 4,
)

d1 >> play('x-{(#r)[WW]}')
d2 >> play('|!{25}| ', dur=18)

d3 >> play('--- ', dur=PDur(4, 9))
d4 >> play('*** ', dur=PDur(3, 6)*3, amp=linvar([.4, 1], 8))

b1 >> birdy(
    [P[c.tonic, c.dominant] for c in  C['Eb7M/6, Dbm7/-5, Cm7/+9, Bbm7/4/9']],
    dur=PDur(3, 11) * 2,
)
b2 >> pluck(
    b1.degree,
    dur=PDur(3, 11) * 2,
    amp=.6
)

b3 >>  cicada(sus=.5, dur=PDur(3,8))

b3.stop()


# ----

x1 >> keys(
    var(C['Cm7, Asus7, F6, Em-5'], 4),
    dur=PDur(3, 8),
)
d2.stop()
d1 >> play('E', dur=PDur(3, 6)*8)
d4 >> play('|Z2|', dur=PDur(3, 8)*2, amp=.4)

x1.dur= PDur(3, 8) * 2

x1.dur= PDur(3, 8)
