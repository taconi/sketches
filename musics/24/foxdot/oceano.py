from FoxDotChord import PChord as p

Scale.default = 'chromatic' 

har = p['D, G7M, A7, A#°, Bm, Bm7M, Bm7, Bm6, Am7, D7(9), Gm7, C7(9), F#m7, B7(9-), E7(9), A7(4)']

c1 >> sitar(var(har, 3), dur=PDur(3, 6), amp=.8)

c2 >> epiano(c1.degree, dur=3, oct=5)

s1 >> play('Ppp', dur=1)


c1.dur = PDur(3, 6) / 2
