from FoxDotChord import PChord as c


Clock.bpm = 180


har = c['Dm7', 'C7/9', 'Bb7/9', 'A7']

x1 >> keys(var(har, 8), dur=var([(PDur(7, 12)*2, PDur(3, 5))], 8))

d1 >> play('--- ')
d2 >> play('V   ')
d3 >> play('+xo+')
d4 >> play('({!C}(ES))   ')

x2 >> sosbell(x1.degree, dur=PDur(7,9)*2, amp=.7)



x1.dur = PDur(7, 9) * 2
d4 >> play('(C(ES))   ')
Group(d1, d2).stop()



har2 = c['F#6, Dm7/9, Bm5/7']

x1.amp = .7
x2.stop()
x3 >> swell(var(har2, [4, 8, 4]), dur=PDur(5, 9)*2, oct=var([4, 5], 16), amp=1.5)

d5 >> play('pn u', amp=2)
