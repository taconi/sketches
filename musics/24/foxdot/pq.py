from FoxDotChord import PChord as C
Scale.default = 'chromatic'

x1 >> play(P['X '].amen(), dur=2)
x2 >> play(P[' {s[---]}'].bubble(), dur=2)

v1 >> space(
    var(C['Ddim, Fm6, G7M, Gbsus'], [2, 2, 4, 4]),
    dur=PDur(7, 9) * 4,
)

b1 >> bass(v1.degree, dur=PDur(7, 9) * 2, sus=.5, amp=.8)

