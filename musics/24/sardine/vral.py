clock.tempo = 120
V.t, V.b, V.f = 0, 'lt', 'electro1:12'
@swim
def tucha(p=1/4, i=0):
    t = V.t
    t += 1
    if t >= 128:
        V.b, V.f = 'lt', 'electro1:12'
        t = 0
    elif t >= 64:
        V.b, V.f = 'dr:7', 'lt'
    V.t = t
    D('.!2 (getA b) .!2 (getA b) .!3 (getA b) . (getA b) .!3 (getA f)', i=i)
    again(tucha, p=1/4, i=i+1)

Pb * d('breath', p='4!3 20', amp=3)
Pd * d('insect:[1 2]!!2', amp=1, latency=.2, p=16)

Pa * d('drum:1 .. drum:1 .. drum:1 . bin . drum:1 . drum:1 ...', p=1/2)


Pa * d('drum:2 .. drum:1 .. drum:3 . bin . drum:5 . drum:1 ...', p=1/2)

