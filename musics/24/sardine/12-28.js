clock.tempo = 150

Pa * d(
  'dr:7 .. dr:7 .. dr:7 . dr .dr:7 . dr:7 ...',
  p=1/4,
)

Pb * zd(
  'superpiano',
  'w. [<vii^M9 vii^7sus4>] '
  'r c [i^m7b5 iii^a iv^m11 ii^m9]',
  p=4, leg=10, amp=.2
)


Pc * zd('linnhats', '<a a h> [0 0] r', p=.25)
Pd * d('dr .', p=.25)
Pe * zd(
  'tabla!6 odx sn', '(iv^6 ii^m v^7 i^M7 vii^m7b5)<3,5>',
  p=.25,
)

Pa.stop()
Pc.stop()
Pd.stop()
Pe.stop()


Pf * d(
  'drum:1 .. drum:1 .. drum:1 . bin . drum:1 . drum:1 ...',
  p=.5, room=.75,size=1,leg=1.5
)


Pf * d(
  'drum:2 .. drum:1 .. drum:3 . bin . drum:5 . drum:1 ...',
  p=.5, room=.75,size=1,leg=1.5
)



Pb * d(
  '(eu superpiano 3 5)',
  n='[(eu D@min7 C@min7 Bb@min7 A@seven 3 8)]!32'
  '[{D@min7}!32 {C@min7}!32 {Bb@min7}!32 {A@seven}!32]!8',
  p=1/4,
)


Pg * d('breath', p='4!3 20', amp=3)
