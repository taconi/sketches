@swim
def vixi(p=.25, i=0):
    ZD('linnhats', '<a a h> [0 0] r', i=i)
    D('dr .', i=i)
    ZD('tabla!6 odx sn', '(iv^6 ii^m v^7 i^M7 vii^m7b5)<3,5>', i=i)
    again(vixi, p=.5, i=i+1)


Pd * zd(
    'superpiano',
    'w. [<vii^M9 vii^7sus4>] r c [i^m7b5 iii^a iv^m11 ii^m9]',
    p=4,
    leg=10,
    amp=.2
)
