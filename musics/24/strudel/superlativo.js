/*
@title Superlativo
@by taconi
@license CC
@created 24/11/2024

open the web editor: https://strudel.cc/?6DD82cDCOEmR
place the cursor over the code by clicking the mouse
and press Ctrl+Enter to run it

replace _$ with $ to enable the pattern
change the code, comment, uncomment, change the effects and have fun

----

abra o editor web: https://strudel.cc/?6DD82cDCOEmR
coloque o cursor sobre o código clickando com o mouse
e pressione Ctrl+Enter para roda-lo

troque  _$ por $ para habilitar o padrão
altere o código, comente, descomente, mude os efeitos e se divirta
*/

await initHydra()

noise(3, 0.4).out(o1)
shape(3, .3, 3)
// .rotate(()=>time/120)
// .repeatY(() => a.fft[0]*8)
// .luma(-1)
// .hue(-1)
// .scale(.3)
.out(o2)
gradient(() => a.fft[0]*4).out(o3)

voronoi(25, 2, 25)
.color(4,[1,4].fast(5),2)
.add(src(o1))
.add(src(o2))
.add(src(o3))
.brightness([0,.5-1,-1.5,-2].smooth())
.out(o0)

// ---

$: s("[bd <hh rim> cp!2](3,8)").color("cyan")
.bank("<LinnDrum!2 KorgT3>")
.echo(2, 2, 2)
.room(1)
.lpf("4000:10")
._pianoroll({labels:1})

$: note("[c gb f bb](3,[8 11])").color("magenta")
.s("gm_piano")
.lpf("4000").lpr("20")
.vib(2)
._pianoroll({labels:1})

_$: note("[c gb f bb]/4(3,11)").color("[yellow red]/2")
.s("gm_electric_bass_pick,gm_bassoon,gm_acoustic_bass")
// .gain(1)
.legato("4")
.chop("[3 8]")
.scope()
._pianoroll({labels:1})

let c="[C^7 Gb7 F^7 <Bbm Bb Bb7 Bb6>]/4"

_$: chord(c).color("#641504")
.s("gm_electric_guitar_clean!5 <gm_agogo@2000 gm_agogo!2>![8,16]")
.euclid("[3, 4]","[6, 10]")
.voicing()
.gain(.4)
.scope()
._punchcard()

_$: chord(c).color("green")
.s("gm_baritone_sax,gm_alto_sax")
.voicing()
.hpf("700:10")
.gain("[1 1.5 1 .5]/4(4,9)")
.pan("[.5 0 1 0]/4,[0 1 0.5 .5]/4")
._pitchwheel()
