/*
@title Antes da meia noite
@by taconi
@license CC
@created 25/11/2024

https://strudel.cc/?8Hg16IdekLrw
*/

await initHydra()

voronoi(8,1,[20,29].fast(.4))
.out(o1)


noise([1,2,3,4,5,6,7,8,8,9,10].smooth(2), [0.05, 0.01])
.kaleid([3, 5, 8].smooth(2))
.color(.2, 1, [-2, 1,6].fast(.4))
.repeat(2, 2)
.luma()
.scale(1)
.add(src(o1).repeat(4).rotate(2))
.out()


// -----------------
Falou valeu

samples('shabda:bass:4')

$:
s("<bd - bd!2>*4,<hh>*6").color("<yellow red>")
.gain(".5")

let scale = "<Eb F# Ab>:<minor![5 4] lydian!3>"

$:
n("<[0,2,4,6] [3,5,7,<8!3 12!2>]>").color("cyan")
.gain("<.2!3 .6 .8>/2")
.chop("<1 2.5>")
.legato("10")
.scale(scale)
.s("bass,gm_sitar")
.color("magenta")
.lpf("2000:15").hpf("<250 500!2 700!2>")

$:
n("<[0,2,4,6] [3,5,7,<8!3 12!2>]>").color("cyan")
.scale(scale)
.s("gm_piano").gain(.5).vib(".5").room(".5").chop("1 3")

$:
s("<lt!2 - perc jazz [rim,cp] >*4,[cp]*[4 6]")
.bank("YamahaRM50").color("blue").lpf("[2000 5000]/4:[1 .. 10]/8")
