/*
@title Piu
@license CC BY-NC-SA
@by taconi
@created 20/11/2024
*/

// abra o editor web: https://strudel.cc/?Inep4wX1T2Ts
// coloque o cursor sobre o código clickando com o mouse
// e pressione Ctrl+Enter para roda-lo

await initHydra()

bpm=120/6

osc(80, [50, 70].smooth(.2), 0.8)
  .pixelate([20, 5].fast(0.5), [30, 10].fast(0.1))
  .blend(
    noise()
      .repeat( () => 1.5*Math.sin(time*0.08) )
      .repeatY([3.5,3.9,4,6,3].fast(.05))
      .kaleid(600)
      .rotate(()=>time/20)
      .color(4, [2.5,0,6].fast(.13))
      .scale([.3, .5, .8, .5].smooth())
      .posterize([5,2, 0.6, 1].smooth())
  )
  .out(o0)

let ch=`<
Am^7 F7#11 Bm7b5 [E7/13 E7b13]
Am^7 F7#11 Bm7b5 [E7/13 E7b13]
Am^7 F7#11 Bm7b5 [E7/13 E7b13]
Am^7 F7#11 Dm7/9 G7!0.5 F7#11!0.5
Em7b5!2 A7b13 Eb7/9
Dm7/9!2 G7 Db7
C^7#5 C^7 F7#11!2
Bm7b5!2 E7b13!2
>`

$: chord(ch)
  .euclid("3", "8")
  .voicing()
  .room(1)
  .adsr(".2:.3:1.4:[1.5 .5]/4")
  .bpf("<100 200 300 400>/2")
  .pan("<1 .5 0 .5>")
  .s("gm_acoustic_guitar_nylon")

$: chord(ch)
  .voicing()
  .s("gm_epiano2")
  .room(.5)
  .hpf(700)
  .pan("<.5 1 .5 0>")
  .lpf("[3000:20 15000:50]/16")

$: chord(ch)
  .voicing()
  .euclid("[4 6]", "8")
  .s("gm_bird_tweet")
  .pan("<0 .5 1 .5>")
  .delay(.25)
  .lpf(4000)
  .gain([.5, 1, 2])

$: chord(ch)
  .euclid("<[12!2 14] 8!2>", "16")
  .pan("<.5 0 .5 1>")
  .s("gm_trombone")

$: s("hh <hh hh:2> <[oh <lt [bd cb]>] mt>, tb:[0 .. 6] -")
  .pan("<1 .5 0 .5>")
  .bank("YamahaRM50")
  .euclid("9", "11")
