/*
@title Fio
@license CC BY-NC-SA
@by taconi
@created 21/11/2024
*/

// abra o editor web: https://strudel.cc/?6DD82cDCOEmR
// coloque o cursor sobre o código clickando com o mouse
// e pressione Ctrl+Enter para roda-lo

await initHydra()

bpm=120/6

noise()
  .repeat( () => 1.5*Math.sin(time*0.08) )
  .repeatY([3.5,3.9,4,6,3].fast(.05))
  // .kaleid(600)
  // .rotate(()=>time/20)
  .color(4, [2.5,0,6].fast(.13))
  .scale([.3, .5, .8, .5].smooth())
  // .posterize([5,2, 0.6, 1].smooth())
  .out(o0)


let ch=`<
Am^7 F7#11 Bm7b5 [E7/13 E7b13]
>`

$: chord(ch)
  .euclid("<[12!2 14] 8!2>", "16")
  .pan("<.5 0 .5 1>")
  .distort(".5 1")
  .delay("<1 .5>")
  .s("gm_trombone  gm_electric_bass_finger")
  .gain("<0 1>")

$: s(`
hh <hh hh:2> <[oh <lt [bd cb]>] mt>,
tb:[0 .. 6] -
`)
  .pan("<1 .5 0 .5>")
  .bank("EmuSP12")
  .euclid("<9>", "11")
  .crush("<3 4 6 8>")
  .gain("<.5 1!3>")
  .scope()

$: chord(ch)
  .voicing()
  .early(.5)
  .s("gm_agogo").room(.7).hpf("[700 1000]/4")
  .lpf("[17000:30 9000]/2")
  .euclid("[9!16 8!16 7!3]/2", "[11!16 12!16 16!3]/24")
  .gain("<.25!15 .5!15 .75!15 1!120 0!60>")
