/*
@title Má-Lida
@by taconi
@license CC BY-NC-SA
@created 22/11/2024
*/

// abra o editor web: https://strudel.cc/?Q_ZBY-eAZwaD
// coloque o cursor sobre o código clickando com o mouse
// e pressione Ctrl+Enter para roda-lo

await initHydra()

osc(3, 0.2, 8)
  .rotate(2)
  .repeatY(()=> time())
  .repeatX(()=> time/20)
  .rotate(()=>time/100)
  .luma()
  .out(o0)

// src(o0).rotate(.01).out() // uncomment after you already have a drawing on the screen


let b01 = note(`[
e2 b2 c!2   a#2 f#2
e2 b2 c!2   e c b2!2
]/12`)
  .sound("gm_cello,gm_tuba")
  .room(1)
  .lpf(9000)
  .hpf(600)
  // .vib("2:1")


let t01 = sound("tambourine")
  .echo(2, 2, 1)
  .room(1)
  .slow(4)
  .delay(2)
  .vib("1:1")

let t02 = sound("<rim -!2>")
  .bank("RolandCompurhythm1000")
  .room(.7)
  .vib("2:1")
  .delay(2)
  .decay(2)
  .slow(2)

let n01 = n("<2>")
  .s("gm_breath_noise")
  .lpf("3000:40")
  .hpf(900)
  .crush("42")
  .euclid("<4!3 2!2 <6 [7 6]*2 8 7>>", "<12!5 16>").fast(2)
  .room(1)
  .delay("1:2")
  .gain(0)


let b11 = note(`
[e2 eb2 d2 db2 c2 b1 bb1 b1!2
]/8`)
  .sound("gm_cello,gm_tuba")
  .room(1)

//   how to invert the bass using chord("Em/D")?
// let p11 = chord('Em Em/Eb Em/D Em/Db C^7 C^7/B C^7/Bb B7!2')
let p11 = note(`[
[e, g, b]
[eb, e, g, b]
[d, e, g, b]
[db, e, g, b]
[c, e, g, b]
[b2, c, e, g, b]
[bb2, c, e, g, b]
[b2, d#, f#, a]
[b2, d#, f#, a]
]/8`)
  .sound('piano')

let p12 = note(`[
[e g b e4]*2
[eb e g b]*2
[d e g b]*2
[db e g b]*2
[c e g b]*2
[b2 c e b]*2
[bb2 c e b]*2
[b2 d# f# a b d#4 f#4 a4]
[b4 d#5 f#5 a5 b5 d#6 f#6 a6]
]/8`)
  .sound('piano')
  .pan(.5, 0, 1, 0)
  .room(1)

//
// $: stack(b01, t01, t02, n01) // intro
// $: stack(b11, t01, t02, p11) // ponte
$: stack(b11, t01, t02, p11, p12) // parte 1
// hush() // fim
