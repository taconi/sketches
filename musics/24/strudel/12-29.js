// https://strudel.cc/?q4VPxlVMeMGx

await initHydra({detectAudio:true,feedStrudel:1})

let p = "<<0!100 1!30 -2!50 3!20> 2 4 <[6,8] [7,9]>>*[0!40 6!70]"
let h = H("[<1 2 -2>(3,8)]/4")

voronoi(h, 0.002, h)
  .color(H("-1 -2"), H("<.2 1>!2"), H("<90!20 20!5 5!5 10!5>"))
  .modulateRotate(osc(9, [0.002, 0.03, 0.3].ease('easeOutQuad')), h)
  .diff(src(o0).scale(0.9).modulateKaleid(osc(),4).modulateScale(shape(4)))
  .scrollY(h, .01)
  .scrollX(h, .5)
  .rotate(h)
  .scale(h)
  .out(o1)

solid(H("<1.5>"), H("2"), H("<-2>"))
  .diff(
    src(o1)
    .scale(H("<-.9!4 -.5!20>/10"))
    .modulateKaleid(shape(H("[<<3!6 3!9> <.6 [.2 .5]>>(4,11)]/16")))
    .modulateScrollY(osc(H("[<<7!20 3> [.9 .1]>(3,8)]/16")))
    .colorama(H("<4!8 2!5 1.5!3>"))
  )
  .diff(voronoi(h,0.3,H("<90 50!5 30!20 20!20>")).colorama(1.9))
  .modulateRepeat(
    shape(h, H("<.8 .09>"))
    .scale(H("<.2 .5>/10"))
    .modulateRepeat(osc(H("<3 4, 2 9>")))
    .colorama(4)
  )
  .add(
    shape(H("[<3 4>(3,11)]*4"))
    .colorama(H("<-1 4 1.3>!8"))
    .kaleid(H("<3 2>*4"))
    .modulateScrollX(
      voronoi().color(2).hue(-2).luma(2),
      x=>time/20
    )
  )
  .scale(H("<10 5 2 20>"))
  .modulatePixelate(
    gradient(H("<20 10>")),
    H("<<10 40!20>(3,11)>/2"),
    [0.002, 0.03, 0.3].ease('easeOutQuad'),
  )
  .brightness(-.2)
  .out(o0)

$: n(p)
.scale(`<
<-!20 Cb:minor!60>
<D#!3 E2!4>:<[<lydian!20 mixolydian!6> major] minor>
>`)
.sound("piano").cpm(60)

$: s("<bd - - [ht -@4 bd] - - sd - sd - bd - - - >*8,[ - cb] - cp")
.rarely(ply("2")).bank("RolandCompurhythm1000")
