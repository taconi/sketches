/*
@title Saudade N⁰0
@license CC BY-NC-SA
@by taconi
@created 20/11/2024
*/

// abra o editor web: https://strudel.cc/?oCV7VBlFARLe
// coloque o cursor sobre o código clickando com o mouse
// e pressione Ctrl+Enter para roda-lo

await initHydra()

noise(30, 0.01, 0.25)
  .pixelate(50, 40)
  .colorama(1.37)
  .out()

$: chord("<C^7#5 F7#11 Am^7 [G^7 Db7#9]>")
  .dict('ireal')
  .voicing()
  .room(0.2)
  .hpf([200, 600, 700].fast(2))
  .s("piano")


$: sound("bd hh <sd sd:1> [<oh [oh:1 oh:2] [hh:4 bd:3]> bd!2]")
  .lpf(10000)
  .hpf(300)
  .bank("YamahaRY30")
